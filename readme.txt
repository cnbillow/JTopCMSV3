
************************** 部署环境 ************************** 


Java版本：    JDK1.7+

数据库：      MySQL 5.1+

支持软件：    Tomcat7+
 


************************** 部署方式 ************************** 

1.将解压后的JTopCMSV3开源版本导入到eclipse工程

2.部署到tomcat（请尽量使用国内maven仓库，以免jar包下载失败导致编译部署不成功）

2.准备好启动数据库，将initData中的数据库脚本执行


	[注意!]：Mysql5.5版本以及更高版本请按照以下说明配置后再安装，否则会导致系统运行某些功能错误！
    
        方法1：在Mysql配置文件my.ini最后加上 	

		sql_mode=STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION

               保存后重新启动MySql服务即可。

        
        方法2： 进入mysql命令行执行 

		SET global sql_mode=`STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION`;


3.数据库成功初始化后，在WEB-INF/config/cs.properties配置数据库配置信息,以下为关键项说明
      
        back_domain        (CMS后台地址,默认为不带端口的IP或域名)
	back_port          (访问端口号)
	back_context       (部署上下文,若为ROOT目录则留空)

        db_name            (数据库名称)

	db_ip              (数据库IP)
	db_port            (数据库端口)

	db_user            (数据库用户名)
	db_pw              (数据库密码)

      
       

                  
 
4.安装完成， 启动tomcat ，进入http://127.0.0.1:8080/login_page,默认用户和密码 admin  jtopcms



************************** 官方联系方式 ************************** 


主页：    http://www.jtopcms.com/

邮箱:     service@jtopcms.com

 




